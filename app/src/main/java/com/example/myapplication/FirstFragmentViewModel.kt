package com.example.myapplication

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController

class FirstFragmentViewModel:ViewModel() {
    val editTextLifeData = MutableLiveData("ddd")
    lateinit var  navController:()->NavController
    fun onButtonClick(view:View){
        navController().navigate(R.id.action_FirstFragment_to_SecondFragment)

    }
}